package factoryPattern;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


@SuppressWarnings("serial")
public class hitesh_Factory_Pattern extends JPanel 
{
   // two buttons for abstract/factory pattern	
	JButton button1, button2;
	
	// popup menu when button is pressed
	JPopupMenu pop;
	
	
	//panel used in factory panel
  static  JPanel extraPanel, displayPanel;
  
  // factory pattern components
  JLabel lab1,lab2;
	
	JButton button;
	
	// abstract factory components
	JLabel lab3,lab4,lab5;
	
    
    //----constructor
	public hitesh_Factory_Pattern() 
	{
   
		 try
		 {
			    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
			    {
			        if ("Nimbus".equals(info.getName())) {
			        	
			            UIManager.setLookAndFeel(info.getClassName());
			            break;
			        }
			    }
			} catch (Exception e) {
			   
			}
		 
		 
		 extraPanel = new JPanel();
		 extraPanel.setLayout(new GridLayout(0,1,5,5));
		 button1 = new JButton("Factory Pattern");
		 button1.addActionListener(new ActionListener() 
		 {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
			String name = button1.getText();
				createPopUp(name);
                 displayPanel.setLayout(null);
				displayPanel.setLayout(new GridLayout(0,1,0,0));
				
				JLabel f1 = new JLabel("Factory Method :");
				f1.setForeground(Color.blue);
				displayPanel.add(f1);
				displayPanel.add(new JLabel(" Defines an interface for creating objects, but let subclasses to decide which class to "));
				displayPanel.add(new JLabel("instantiate and Refers to the newly created object through a common interface"));
				
				JLabel f2 = new JLabel("Factory(Simplified version of Factory Method):");
				f2.setForeground(Color.blue);
				displayPanel.add(f2);
				
				displayPanel.add(new JLabel(" Creates objects without exposing the instantiation "));
				displayPanel.add(new JLabel("logic to the client and Refers to the newly created object through a common interface."));
                
				JLabel f3 = new JLabel("Example:");
				f3.setForeground(Color.blue);
				displayPanel.add(f3);
				displayPanel.add(new JLabel(" Imagine you are constructing a house and you approach a carpenter for a door. You give the"));
                displayPanel.add(new JLabel(" measurement for the door and your requirements, and he will construct a door for you. In this case, the "));
                displayPanel.add(new JLabel(" carpenter is a factory of doors. Your specifications are inputs for the factory, and the door is the output"));
                displayPanel.add(new JLabel("or product from the factory."));	
              
                final JLabel next = new JLabel("Continue");
                next.setForeground(Color.BLUE);
                next.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
					next.setBorder(null);	
					}
					
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						next.setBorder(BorderFactory.createEtchedBorder(Color.blue, Color.white));
					}
					
					@Override
					public void mouseClicked(MouseEvent e) {

						displayPanel.removeAll();
						displayPanel.setLayout(new BorderLayout());
						

						try {
						BufferedImage	wPic = ImageIO.read(this.getClass().getResource("factory_pattern_uml_diagram.jpg"));
						JLabel wIcon = new JLabel(new ImageIcon(wPic));
						displayPanel.add(wIcon, BorderLayout.NORTH);

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						final JLabel next = new JLabel("Continue");
						next.setForeground(Color.blue);
						next.addMouseListener(new MouseListener() {
							
							@Override
							public void mouseReleased(MouseEvent e) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void mousePressed(MouseEvent e) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void mouseExited(MouseEvent e) {
								// TODO Auto-generated method stub
							next.setBorder(null);	
							}
							
							@Override
							public void mouseEntered(MouseEvent e) {
								next.setBorder(BorderFactory.createEtchedBorder(Color.blue, Color.white));
								
							}
							
							@Override
							public void mouseClicked(MouseEvent e) {
								displayPanel.removeAll();
								displayPanel.setLayout(new GridLayout(0,1,0,0));
								displayPanel.setBorder(BorderFactory.createEmptyBorder(50, 200, 50, 200));
								
								lab1 = new JLabel("Enter your request:");
								lab2 = new JLabel();
								final JComboBox box = new JComboBox();
								box.addItem("Select Items");
								box.addItem("CIRCLE");
								box.addItem("Square");
								box.addItem("RECTANGLE");
								
								
								button = new JButton("Draw");
								button.addActionListener(new ActionListener() {
									
									@Override
									public void actionPerformed(ActionEvent e) {
										
										if(box.getSelectedItem().equals(""))
										{
											JOptionPane.showMessageDialog(null, "Empty Field!");
											return;
										}else
										{
											String ss = (String) box.getSelectedItem();
										  shapeFactory S = new shapeFactory();
										  shape shape1 = S.getShape(ss);
										  lab2.setText(shape1.draw());
										  
										}


									}
								});
								
								displayPanel.add(lab1);
								displayPanel.add(box);
								displayPanel.add(button);
								displayPanel.add(lab2);
								
								
								displayPanel.revalidate();
								displayPanel.repaint();
								
								
							}
						});
						displayPanel.add(next, BorderLayout.SOUTH);
						
						displayPanel.revalidate();
						displayPanel.repaint();
						
					}
				});
                displayPanel.add(next);
				
				

				
				
			}
			
		});
		 
		 button2 = new JButton("Abstract Factory Pattern");
		button2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				String name = button2.getText();
				createPopUp(name);
				displayPanel.setLayout(null);
				displayPanel.setLayout(new GridLayout(0,1,0,0));
				JLabel f1 = new JLabel("Abstract Factory : ");
				f1.setForeground(Color.BLUE);
				displayPanel.add(f1);
			    
				displayPanel.add(new JLabel("A factory that creates other factories, and these factories in turn create objects derived "));
				displayPanel.add(new JLabel("from base classes. You do this because you often don't just want to create a single object")); 
				displayPanel.add(new JLabel("as with Factory method) - rather, you want to create a collection of related objects."));
				displayPanel.add(new JLabel("Offers the interface for creating a family of related objects, without explicitly specifying their classes."));
				
				JLabel f2 = new JLabel(" Example : ");
				f2.setForeground(Color.BLUE);
				displayPanel.add(f2);
			
				displayPanel.add(new JLabel("Now, consider the same example of the door. You can go to a carpenter, or you can go to a plastic door  "));
				displayPanel.add(new JLabel(" shop or a PVC shop. All of them are door factories. Based on the situation, you decide what kind of  "));
				displayPanel.add(new JLabel(" factory you need to approach. This is like an Abstract Factory."));
				
				final JLabel next = new JLabel("Continue");
				next.setForeground(Color.BLUE);
				next.addMouseListener(new MouseListener() {
					
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						next.setBorder(null);
					}
					
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
					next.setBorder(BorderFactory.createEtchedBorder(Color.blue, Color.white));	
					}
					
					@Override
					public void mouseClicked(MouseEvent e) {
						displayPanel.removeAll();
						displayPanel.setLayout(new BorderLayout());

						try {
						BufferedImage	wPic = ImageIO.read(this.getClass().getResource("abstractfactory_pattern_uml_diagram.jpg"));
						JLabel wIcon = new JLabel(new ImageIcon(wPic));
						displayPanel.add(wIcon, BorderLayout.CENTER);

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						final JLabel next = new JLabel("Continue");
						next.setForeground(Color.blue);
						next.addMouseListener(new MouseListener() {
							
							@Override
							public void mouseReleased(MouseEvent e) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void mousePressed(MouseEvent e) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void mouseExited(MouseEvent e) {
								// TODO Auto-generated method stub
							next.setBorder(null);	
							}
							
							@Override
							public void mouseEntered(MouseEvent e) {
								// TODO Auto-generated method stub
							next.setBorder(BorderFactory.createEtchedBorder(Color.blue, Color.white));	
							}
							
							@Override
							public void mouseClicked(MouseEvent e) {
								// TODO Auto-generated method stub
								displayPanel.removeAll();
								displayPanel.setLayout(new GridLayout(0,1,10,10));
								displayPanel.setBorder(BorderFactory.createEmptyBorder(30,170, 30, 170));
								
								lab3 = new JLabel("Select items:");
								lab4 = new JLabel();
								lab5 = new JLabel();

								final JComboBox box1 = new JComboBox();
								box1.addItem("Select Items");
								box1.addItem("Circle");
								box1.addItem("Rectangle");
								box1.addItem("Square");
								
								final JComboBox box2 = new JComboBox();
								box2.addItem("Select items");
								box2.addItem("Black");
								box2.addItem("Blue");
								box2.addItem("Green");
								
								JButton boton = new JButton("Draw/Fill");
								boton.addActionListener(new ActionListener() {
									
									@Override
									public void actionPerformed(ActionEvent e) {

										String sape = (String) box1.getSelectedItem();
										String clr  = (String) box2.getSelectedItem();
										
										abstractClass shapeFactory = factoryProducer.getFactory("shape");

									      //get an object of Shape Circle
									      shape shape1 = shapeFactory.getShape(sape);

									      //call draw method of Shape Circle
									      lab4.setText(shape1.draw());


									      abstractClass colorFactory = factoryProducer.getFactory("cOLOR");

									      //get an object of Color Red
									      color color1 = colorFactory.getColor(clr);

									      //call fill method of Red
									      lab5.setText(color1.fill());


										
									}
								});
								
								displayPanel.add(lab3);
								displayPanel.add(box1);
								displayPanel.add(box2);
								displayPanel.add(boton);
								displayPanel.add(lab4);
								displayPanel.add(lab5);
								
								
								displayPanel.revalidate();
								displayPanel.repaint();
							}
						});
						
						
						displayPanel.add(next, BorderLayout.SOUTH);
						displayPanel.revalidate();
						displayPanel.repaint();
						
					}
				});
				displayPanel.add(next);
			}
		});
		 
		 
		 
		 extraPanel.setBorder(BorderFactory.createEmptyBorder(110, 190, 110, 190));
		 
		 extraPanel.add(button1);
		 
		 
		 extraPanel.add(button2);
		 
		super.setLayout(new BorderLayout()); 		 
		 super.add(extraPanel, BorderLayout.CENTER);
		 
		 
		 
		 
		 
		 
	}
	
	public void createPopUp(String name)
	{
		
		pop = new JPopupMenu()
		{
			 public void paint(Graphics g)
			 { 
	                Graphics2D g2d = (Graphics2D) g.create(); 
	                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f)); 
	                super.paint(g2d); 
	                g2d.dispose(); 
	        } 
		};
		pop.setLocation(400, 230);
		pop.setBorder(null);
		pop.setOpaque(false);
		
		JPanel displayPanel1= new JPanel()
		{
			protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            int w = getWidth(); 
            int h = getHeight();
            Graphics2D g2d = (Graphics2D) g;
            g2d.setPaint(new GradientPaint(0, 0, Color.BLUE, 0, h, Color.WHITE));
            g2d.fillRect(0, 0, w, h);
        }};
		displayPanel1.setPreferredSize(new Dimension(10	,20));
		displayPanel1.setLayout(new BorderLayout());
		pop.add(displayPanel1);
		
		
		JButton exit = new JButton("X");
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				pop.removeAll();
				pop.revalidate();
				pop.repaint();
				pop.setVisible(false);
				
				
			}
		});
		exit.setBackground(Color.white);
		exit.setForeground(Color.red);
		displayPanel1.add(exit, BorderLayout.EAST);
		
		JLabel lab = new JLabel(name);
		displayPanel1.add(lab, BorderLayout.CENTER);
		displayPanel1.add(new JLabel("                                                                               "), BorderLayout.WEST);
		
	    displayPanel = new JPanel(){
	    	
	    	protected void paintComponent(Graphics g) {
	            super.paintComponent(g);
	            int w = getWidth(); 
	            int h = getHeight();
	            Graphics2D g2d = (Graphics2D) g;
	            g2d.setPaint(new GradientPaint(0, 0, Color.RED, 0, h, Color.WHITE));
	            g2d.fillRect(0, 0, w, h);
	        }
	    };
	
		displayPanel.setPreferredSize(new Dimension(570,300));
		
		
		
		
		pop.add(displayPanel);
		pop.setVisible(true);
		pop.repaint();
		

		
		
	}
	
	
}
