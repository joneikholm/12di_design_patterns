package factoryPattern;

public class Rectangle implements shape {

	@Override
	public String draw(){

		return "Drawing Rectangle";
	}

}
