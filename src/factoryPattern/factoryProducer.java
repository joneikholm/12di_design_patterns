package factoryPattern;

public class factoryProducer 
{
	public static abstractClass getFactory(String choice){
	      if(choice.equalsIgnoreCase("SHAPE")){
	         return new shapeFactory();
	      } else if(choice.equalsIgnoreCase("COLOR")){
	         return new colorFactory();
	      }
	      return null;
	   }


}
