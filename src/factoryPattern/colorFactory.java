package factoryPattern;

public class colorFactory extends abstractClass {

	@Override
	color getColor(String color) {
		 if(color == null){
	         return null;
	      }		
	      if(color.equalsIgnoreCase("Black")){
	         return new Black();
	      } else if(color.equalsIgnoreCase("GREEN")){
	         return new Green();
	      } else if(color.equalsIgnoreCase("BLUE")){
	         return new Blue();
	      }
	      return null;

		
		
	}

	@Override
	shape getShape(String shape) {
		// TODO Auto-generated method stub
		return null;
	}

}
