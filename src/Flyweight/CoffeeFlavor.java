package Flyweight;

class CoffeeFlavor implements CoffeeOrder {
    private final String flavor;
 
    public CoffeeFlavor(String newFlavor) {
        this.flavor = newFlavor;
    }
 
    public String getFlavor() {
        return this.flavor;
    }
    public String serveCoffee(CoffeeOrderContext context) {
        return "Serving Coffee flavor " + flavor + " to table number " + context.getTable();
    }
}