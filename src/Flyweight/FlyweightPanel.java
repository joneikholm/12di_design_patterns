package Flyweight;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

//FlyweightFactory object

public class FlyweightPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** The flavors ordered. */
	private static CoffeeFlavor[] flavors = new CoffeeFlavor[100];
	/** The tables for the orders. */
	private static CoffeeOrderContext[] tables = new CoffeeOrderContext[100];
	private static int ordersMade = 0;
	private static CoffeeFlavorFactory flavorFactory;
	JTextArea messageArea;
	private static JButton description;

	public static void takeOrders(String flavorIn, int table) {
		flavors[ordersMade] = flavorFactory.getCoffeeFlavor(flavorIn);
		tables[ordersMade++] = new CoffeeOrderContext(table);
	}

	public FlyweightPanel() {
		// public static void main(String[] args)
		// {
		super();
		setLayout(null);
		setPreferredSize(new Dimension(600, 400));
		messageArea = new JTextArea(8, 40);
		add(new JScrollPane(messageArea)).setBounds(10, 10, 400, 300);

		flavorFactory = new CoffeeFlavorFactory();

		takeOrders("Cappuccino", 2);
		takeOrders("Cappuccino", 2);
		takeOrders("Frappe", 1);
		takeOrders("Frappe", 1);
		takeOrders("Espresso", 1);
		takeOrders("Frappe", 897);
		takeOrders("Cappuccino", 97);
		takeOrders("Cappuccino", 97);
		takeOrders("Frappe", 3);
		takeOrders("Espresso", 3);
		takeOrders("Cappuccino", 3);
		takeOrders("Espresso", 96);
		takeOrders("Frappe", 552);
		takeOrders("Cappuccino", 121);
		takeOrders("Espresso", 121);
		messageArea.append("For exemple: \n"+
		"takeOrders(\"Cappuccino\", 2);\n"+
		"takeOrders(\"Cappuccino\", 2);\n"+
		"takeOrders(\"Frappe\", 1);\n"+
		"takeOrders(\"Frappe\", 1);\n"+
		"takeOrders(\"Espresso\", 1);\n"+
		"takeOrders(\"Frappe\", 897);\n"+
		"takeOrders(\"Cappuccino\", 97);\n"+
		"takeOrders(\"Cappuccino\", 97);\n"+
		"takeOrders(\"Frappe\", 3);\n"+
		"takeOrders(\"Espresso\", 3);\n"+
		"takeOrders(\"Cappuccino\", 3);\n"+
		"takeOrders(\"Espresso\", 96);\n"+
		"takeOrders(\"Frappe\", 552);\n"+
		"takeOrders(\"Cappuccino\", 121);\n"+
		"takeOrders(\"Espresso\", 121);\n\n ");
		for (int i = 0; i < ordersMade; ++i) {
			messageArea.append(flavors[i].serveCoffee(tables[i])+ "\n");
		}
		System.out.println(" ");
		System.out.println("total CoffeeFlavor objects made: "
				+ flavorFactory.getTotalCoffeeFlavorsMade());
		messageArea.append("\n total CoffeeFlavor objects made: "
				+ flavorFactory.getTotalCoffeeFlavorsMade());
		
		
		
		description = new JButton("description");
		description.setBounds(410, 290, 100, 20);
		add(description);
		description.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(
								null,
								" A flyweight is an object that minimizes memory use by sharing as much\n"
								+"data as possible with other similar objects; it is a way to use objects\n"
								+"in large numbers when a simple repeated representation would use an\n"
								+"unacceptable amount of memory. Often some parts of the object state \n"
								+"can be shared, and it is common practice to hold them in external data \n"
								+"structures and pass them to the flyweight objects temporarily when they are used.\n");

			}
		});
	}
}