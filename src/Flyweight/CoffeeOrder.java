package Flyweight;

public interface CoffeeOrder {
    public String serveCoffee(CoffeeOrderContext context);

}
