package Iterator;

import java.util.*;

public class Menu
{
	List<Item> menuItems;

	public Menu()
	{
		menuItems = new ArrayList<Item>();
	}

	public void addItem(Item item)
	{
		menuItems.add(item);
	}

	public Iterator<Item> iterator()
	{
		return new MenuIterator();
	}

	public boolean isEmpty()
	{
		return menuItems.isEmpty();
	}
	
	public int getSize()
	{
		return menuItems.size();
	}

	class MenuIterator implements Iterator<Item>
	{

		int currentIndex = 0;

		public boolean hasNext()
		{
			return !(currentIndex >= menuItems.size());
		}

		public Item next()
		{
			return menuItems.get(currentIndex++);
		}

		public void remove()
		{
			menuItems.remove(--currentIndex);
		}
	}
}
