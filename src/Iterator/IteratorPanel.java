package Iterator;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;

public class IteratorPanel extends JPanel
{
	Menu menu = new Menu();
	static int currentItem = 1;
	static final long serialVersionUID = 1L;
	Iterator<Item> iterator = menu.iterator();

	public IteratorPanel()
	{
		super();

		final JTextArea area = new JTextArea(10, 25);
		JScrollPane scrollPane = new JScrollPane(area);
		final JButton iterateButton = new JButton("Iterate"), removeButton = new JButton("Remove");
		JPanel panel1 = new JPanel(), panel2 = new JPanel(), panel3 = new JPanel(), bigPanel = new JPanel();
		JLabel info = new JLabel("<html>The iterator pattern is a design pattern in which an iterator is used to traverse a container and<br>" +
				"access the container's elements. The iterator pattern decouples algorithms from containers;<br>" +
				"in some cases, algorithms are necessarily container-specific and thus cannot be decoupled.</html>");
		bigPanel.setLayout(new BoxLayout(bigPanel, BoxLayout.PAGE_AXIS));
		iterateButton.setPreferredSize(new java.awt.Dimension(80, 26));
		panel1.add(scrollPane);
		panel2.add(iterateButton);
		panel2.add(removeButton);
		panel3.add(info);
		bigPanel.add(panel1);
		bigPanel.add(panel2);
		bigPanel.add(Box.createVerticalStrut(25));
		bigPanel.add(panel3);
		area.append("Initial list:\n");
		area.setEditable(false);
		this.add(bigPanel);

		menu.addItem(new Item("Chicken soup", 10.0));
		menu.addItem(new Item("French fries", 2.0));
		menu.addItem(new Item("Spaghetti", 15.0));

		while(iterator.hasNext())
		{
			Item item = iterator.next();
			area.append(item.toString() + "\n");
		}
		
		iterateButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				area.append("\nIterating to item #" + currentItem + ":\n");
				
				int itemIndex = currentItem;
				iterator = menu.iterator();
				while(iterator.hasNext())
				{
					Item item = iterator.next();
					
					if(itemIndex == 1)
					{
						area.append(item.toString() + "\n");
						break;
					}
					else itemIndex--;
				}
				
				if(++currentItem > menu.getSize()) currentItem = 1;
				area.setCaretPosition(area.getDocument().getLength());
			}
		});

		removeButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				area.append("\nRemoving the last item.\n");
				iterator.remove();

				area.append("\nAfter iteration:\n");
				iterator = menu.iterator();
				while(iterator.hasNext())
				{
					Item item = iterator.next();
					area.append(item.toString() + "\n");
				}
				
				if(menu.isEmpty())
				{
					area.append("The list is now empty.\n");
					iterateButton.setEnabled(false);
					removeButton.setEnabled(false);
				}

				if(currentItem > menu.getSize()) currentItem = 1;
				area.setCaretPosition(area.getDocument().getLength());
			}
		});
	}
}
