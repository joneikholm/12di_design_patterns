package decorator;

import java.awt.Color;
import java.awt.event.ActionListener;

public class ActionizedDecoration extends DecoratableButton
{
	
	public ActionizedDecoration(ButtonAbstract button, ActionListener listener){
		super(button);
		this.textToAdd = "AC";
		this.button.addActionListener(listener);
		
		updateText();
	}
	
	public void updateText(){
		updateText(textToAdd);
	}

	
}
