package decorator;

import javax.swing.JButton;

public abstract class ButtonAbstract 
{
	public JButton button;
	public boolean previouslyInitialised = false;
	
	
	public ButtonAbstract(){}
	public ButtonAbstract(String text)
	{
		this.button = new JButton();
	}

	
	
	public void updateText(String text)
	{
		this.button.setText(this.button.getText()+" , "+text);
	}
	
	public void updateBounds(int x, int y, int width, int height){
		this.button.setBounds(x, y, width, height);	
	}
	public void updateSize(int width, int height){
		this.button.setBounds(this.button.getX(), this.button.getY(), width, height);	
	}
	public void updateLocation(int x, int y){
		this.button.setBounds(x, y, this.button.getWidth(), this.button.getHeight());	

	}

}
