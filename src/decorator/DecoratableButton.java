package decorator;

import javax.swing.JButton;

public abstract class DecoratableButton extends ButtonAbstract
{
	String textToAdd;
	
	
	public DecoratableButton (ButtonAbstract param)
	{
		this.previouslyInitialised = param.previouslyInitialised;
		this.button                = param.button               ;
		this.textToAdd             = this.button.getText()      ;
		
		if(!previouslyInitialised){
			
			this.button.setBounds(0, 0, 120, 30);
			this.previouslyInitialised = true;
			
		}
		
		
	}	


}
