package decorator;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DecoratorPanel extends JPanel
{

	public DecoratorPanel(){
		
		super();
		
		setLayout(null   );
		setSize  (300, 300);
		
		
		ButtonAbstract first = new Button("First Button");
		first = new BackgroundDecoration(first, Color.RED  );
		first = new ForegroundDecoration(first, Color.BLACK);
		first = new ResizableDecoration (first,210,55);
		first = new ActionizedDecoration(first, new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				JOptionPane.showMessageDialog(null, 
						"By default, buttons don't have action listeners. The action listener addition is \n"
						+ "a decoration added on top of the buttons. \n"
						+ "\n"
						+ "\n"
						+ "\n"
						+ "The button has the following decorations on it :\n"
						+ arg0.getActionCommand());
				
			}
		});
		first.updateLocation(50, 50);
		first.button.setText(first.button.getText().substring(2));
		
		
		
		ButtonAbstract second = new Button("Button");
		second = new ResizableDecoration(second, 150, 20);
		second = new ActionizedDecoration(second, new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{

				JOptionPane.showMessageDialog(null, 
				"The button has the following decorations on it :\n"
						+ e.getActionCommand()
				);				
			}
		});
		second.updateLocation(50, 120);
		second.button.setText(second.button.getText().substring(2));
		
		
		
		
		ButtonAbstract third = new Button("Button");
		third = new BackgroundDecoration(third, Color.CYAN);
		third.updateLocation(50, 150);
		third.button.setText(third.button.getText().substring(2));
		
		
		ButtonAbstract fourth = new Button("Button");
		fourth = new ForegroundDecoration(fourth, Color.GREEN);
		fourth.updateLocation(50,190);

		fourth.button.setText(fourth.button.getText().substring(2));
		
		
		
		JTextArea area = new JTextArea();
		area.setText
		("With the Decorator pattern we can add more stuff to  \neach button, however we want ,by using decorations. \nCurrently there are "
		+"4 decorations to be placed, \nBackground Color Changer ( BG ), Foreground Color \nChanger ( FG ), Resizable ( R ), Actionized ( AC) .");
		area.setEditable(false);
		area.setBackground(new Color(235,235,235));
		area.setBounds(5, 220, 300, 80);
		area.setFont(area.getFont().deriveFont(2));
		area.setFont(area.getFont().deriveFont(11.2f));
		
		
		
		JTextArea secondArea = new JTextArea();
		secondArea.setText
		("Every buttons is basic by default, the way we \ncustomize them is by applying decorations to them");
		secondArea.setEditable(false);
		secondArea.setBackground(new Color(235,235,235));
		secondArea.setBounds(5, 0, 300, 80);
		secondArea.setFont(secondArea.getFont().deriveFont(2));
		secondArea.setFont(secondArea.getFont().deriveFont(11.2f));
		
		
		add(first.button);
		add(second.button);
		add(third.button);
		add(fourth.button);
		add(area);
		add(secondArea);
		
		setVisible(true);
		
	}
	
	
}
