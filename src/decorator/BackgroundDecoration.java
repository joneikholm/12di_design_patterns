package decorator;

import java.awt.Color;

public class BackgroundDecoration extends DecoratableButton
{
	
	public BackgroundDecoration(ButtonAbstract button, Color color){
		super(button);
		this.textToAdd = "BG";
		this.button.setBackground(color);
		
		updateText();
	}
	
	public void updateText(){
		updateText(textToAdd);
	}

	
}
