package decorator;

import java.awt.Color;

public class ForegroundDecoration extends DecoratableButton
{
	
	public ForegroundDecoration(ButtonAbstract button, Color color){
		super(button);
		this.textToAdd = "FG";
		this.button.setForeground(color);
		
		updateText();
	}
	
	public void updateText(){
		updateText(textToAdd);
	}

	
}
