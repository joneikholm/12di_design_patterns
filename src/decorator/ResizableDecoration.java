package decorator;

public class ResizableDecoration extends DecoratableButton
{
	
	public ResizableDecoration(ButtonAbstract button, int width, int height){
		super(button);
		this.textToAdd = "R";
		this.updateSize(width, height);
		updateText();
	}
	
	public void updateText(){
		updateText(textToAdd);
//		this.button.setBounds(10,10,20,20);
	}

	
}
