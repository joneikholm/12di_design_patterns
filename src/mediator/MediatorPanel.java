package mediator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MediatorPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	public MediatorPanel()
	{
		super();

		MediatorClass mediator = new MediatorClass();

		Button b1 = new Button("1", mediator);
		Button b2 = new Button("2", mediator);
		Button b3 = new Button("3", mediator);
		Label label = new Label("Start of program", mediator);

		setLayout(null);
		setSize(300, 300);

		b1.setBounds(50, 50, b1.getWidth(), b1.getHeight());
		add(b1);

		b2.setBounds(150, 50, b1.getWidth(), b1.getHeight());
		add(b2);

		b3.setBounds(250, 50, b1.getWidth(), b1.getHeight());
		add(b3);

		label.setBounds(150, 200, label.getWidth(), label.getHeight());
		add(label);

		JButton helpButton = new JButton("Help");
		helpButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{

				JOptionPane
						.showMessageDialog(
								null,
								"The mediator pattern uses a mediator interface and class that sends a message to all \nof the colleagues (the buttons) as long as they are not the sender. Objects that \nwant to be mediated have to sign up in the mediator class. Those objects have \nto implement a mediatable interface."
									);
			}
		});
		helpButton.setBounds(this.getWidth() - 100, this.getHeight() - 40, 100, 40);
		add(helpButton);

		setVisible(true);

	}

}
