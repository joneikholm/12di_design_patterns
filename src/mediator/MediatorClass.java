package mediator;

import java.util.ArrayList;

public class MediatorClass implements MediatorInterface
{
	public ArrayList<Mediatable> list = new ArrayList<Mediatable>();

	
	
	
	@Override
	public void send(String message, Mediatable sender)
	{
		receive(message,sender);
	}
	

	@Override
	public void receive(String message, Mediatable sender)
	{
		for(int i=0;i< list.size();i++){
			
			if( !sender.equals(list.get(i))){
				
				list.get(i).receive(message);
			}
			
		}
	}



	@Override
	public void signUp(Mediatable mediatable)
	{
		list.add(mediatable);
	}

}
