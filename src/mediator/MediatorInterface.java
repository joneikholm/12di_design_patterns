package mediator;

import java.util.ArrayList;

public interface MediatorInterface
{
	
	public ArrayList<Mediatable> list = new ArrayList<Mediatable>();
	
	public void signUp(Mediatable mediatable);
	public void send(String message, Mediatable sender);
	public void receive(String message, Mediatable sender);
	
	
}
