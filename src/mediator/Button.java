package mediator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Button extends JButton implements Mediatable

{
	private String name;
	private MediatorClass mediator;
	
	public Button(String text, MediatorClass mediator){
		super(text);
		this.name = text;
		this.mediator = mediator;
		this.setSize(100, 100);
		signUp();
		
		
		this.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				send(arg0.getActionCommand());
			}
		});
	}

	@Override
	public void send(String message)
	{
		this.mediator.send("Pressed button #"+message, this);
		this.setEnabled(false);
		
	}

	@Override
	public void receive(String message)
	{
		System.out.println(name+" received "+message);
		this.setEnabled(true);		
	}

	@Override
	public void signUp()
	{
		this.mediator.signUp(this);
		
	}
	
	
}
