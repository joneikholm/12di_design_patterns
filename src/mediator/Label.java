package mediator;

import javax.swing.JLabel;

public class Label extends JLabel implements Mediatable
{
	private MediatorClass mediator;
	private String name;
	
	public Label(String message, MediatorClass mediator){
		
		super(message);
		this.name = message;
		this.mediator = mediator;
		this.setSize(150, 40);
		
		signUp();
	}
	@Override
	public void send(String message)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void receive(String message)
	{
		this.setText(message);
		
	}

	@Override
	public void signUp()
	{
		this.mediator.signUp(this);
	}

}
