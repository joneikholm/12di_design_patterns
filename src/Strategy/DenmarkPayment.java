package Strategy;


public class DenmarkPayment implements PaymentStrategy
{
	private double valutaRate = 1;
	private double incomeTax = 0.15; //top-tax
	private double labourMarketTax = 0.08;
	private double communalTax = 0.25;
	private double healthCareTax = 0.06;
	private double personalExemption = 3500;
	
	@Override
	public double payWage(double amount)
	{
		double totalTax = incomeTax + healthCareTax + labourMarketTax + communalTax;
		
		amount = ((amount * valutaRate) - personalExemption) * (1 - totalTax) + personalExemption;
		
		return amount;
	}
}
