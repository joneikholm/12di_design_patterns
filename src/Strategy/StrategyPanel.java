package Strategy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


@SuppressWarnings("serial")
public class StrategyPanel extends JPanel
{
	
	public StrategyPanel()
	{
		super();
		setLayout(null);
		setPreferredSize(new Dimension(600, 400));;
		
		JButton payButton = new JButton("Pay wage");
		
		final JTextField payField = new JTextField();
		
		String info = "<HTML>Each radio button option represents an object. Depending on which radio button is selected, a corresponding object will be created "
				+ "as a \"paymentStrategy\" for the 'PayWage' method to use. This PayWage method is called when clicking the 'Pay wage'-button. The objects "
				+ "representing the strategy for payment to each country all have different data such as local valuta and how taxes are calculated in the "
				+ "given country. The Strategy Pattern allows us to specify behaviour at runtime instead of having behaviour specific to the method "
				+ "(in this case, the PayWage method).</HTML>";
		
		JLabel infoLabel = new JLabel(info);
		JLabel payLabel = new JLabel("Amount to pay (in DKK)");
		JLabel receivedLabel = new JLabel("Amount received by employee:");
		final JLabel receivedWage = new JLabel();
		JLabel countryLabel = new JLabel("Employee's country");
		
		JRadioButton denmarkButton = new JRadioButton("Denmark");
		JRadioButton romaniaButton = new JRadioButton("Romania");
		JRadioButton unitedStatesButton = new JRadioButton("United States");
		
		denmarkButton.setSelected(true);
		denmarkButton.setActionCommand("DK");
		romaniaButton.setActionCommand("RO");
		unitedStatesButton.setActionCommand("US");
		
		final ButtonGroup group = new ButtonGroup();
	    group.add(denmarkButton);
	    group.add(romaniaButton);
	    group.add(unitedStatesButton);
	    
	    payLabel.setBounds(25, 25, 150, 20);
	    add(payLabel);
	    payField.setBounds(25, 50, 150, 20);
	    add(payField);
	    
	    countryLabel.setBounds(25, 75, 150, 20);
	    add(countryLabel);
	    denmarkButton.setBounds(25, 100, 150, 20);
	    add(denmarkButton);
	    romaniaButton.setBounds(25, 125, 150, 20);
	    add(romaniaButton);
	    unitedStatesButton.setBounds(25, 150, 150, 20);
	    add(unitedStatesButton);
	    
	    receivedLabel.setBounds(25, 175, 175, 20);
	    add(receivedLabel);
	    receivedWage.setBounds(25, 200, 175, 20);
	    receivedWage.setBackground(Color.WHITE);
	    receivedWage.setOpaque(true);
	    add(receivedWage);
	    
	    payButton.setBounds(250, 25, 100, 50);
	    add(payButton);
	    
	    infoLabel.setBounds(250, 85, 300, 200);
	    add(infoLabel);
	    
	    payButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String choice = group.getSelection().getActionCommand();
				System.out.println(choice);
				
				PaymentStrategy strategy;
				String currency;
				
				if (choice.equals("DK"))
				{
					strategy = new DenmarkPayment();
					currency = "DKK";
				} else if (choice.equals("RO")) {
					strategy = new RomaniaPayment();
					currency = "RON";
				} else {
					strategy = new UnitedStatesPayment();
					currency = "USD";
				}
				
				MultiNationalCompany MNC = new MultiNationalCompany();
				double total = MNC.payWage(Double.parseDouble(payField.getText()), strategy);
				total = Math.floor(total * 100) / 100;
				receivedWage.setText(total+" "+currency);
			}
		});
	    
	    setVisible(true);
	}
}
