package Strategy;


public class MultiNationalCompany
{
	public double payWage(double amount, PaymentStrategy method)
	{
		return method.payWage(amount);
	}

}
