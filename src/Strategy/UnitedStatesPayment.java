package Strategy;


public class UnitedStatesPayment implements PaymentStrategy
{
	private double valutaRate = 0.18;
	private double incomeTax = 0.15;
	private double medicareTax = 0.145;
	private double personalExemption = 330;
	
	@Override
	public double payWage(double amount)
	{
		double totalTax = medicareTax + incomeTax;
		
		amount = ((amount * valutaRate) - personalExemption) * (1 - totalTax) + personalExemption;
		
		return amount;
	}
}
