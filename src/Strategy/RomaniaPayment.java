package Strategy;


public class RomaniaPayment implements PaymentStrategy
{
	private double valutaRate = 0.6;
	private double incomeTax = 0.16;
	private double socialSecurityTax = 0.105;
	private double healthFundTax = 0.055;
	private double unemploymentFundTax = 0.05;
	private double personalExemption = 250;
	
	@Override
	public double payWage(double amount)
	{
		double totalTax = incomeTax + socialSecurityTax + healthFundTax + unemploymentFundTax;
		
		amount = ((amount * valutaRate) - personalExemption) * (1 - totalTax) + personalExemption;
		
		return amount;
	}
}
