package Strategy;


public interface PaymentStrategy
{
	public double payWage(double amount); //returns sum (in local currency) received after tax and deductions
}
