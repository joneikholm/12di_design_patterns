package MVCPattern;


public class MVCController {

	private MVCModel model;
	private MVCView view;
	
	public MVCController (MVCView view) {
		this.view = view;
		this.model = new MVCModel();
	}
	 
	public void startShow() {
		if (view.showButton.getText().equals("Showtime")) {
			String[] flexText = model.getFlexText(); 
			
			view.appendFlexareaText(flexText[0]);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for (int  i = 1; i < flexText.length; i++) {
				view.appendFlexareaText("\n" + flexText[i]);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			view.showButton.setText("Continue");
		} else if (view.showButton.getText().equals("Continue")) {
			String[] endText = model.getEndText(); 
			view.clearFlexArea();
			view.vaporizeButton();
			view.appendFlexareaText(endText[0]);
			for (int  i = 1; i < endText.length; i++) {
				view.appendFlexareaText("\n" + endText[i]);
				
			}
			view.addImageToPanel();
		}
	}
}	