package MVCPattern;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class MVCView extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	public JButton showButton = new JButton("Showtime");
	private JTextArea infoFlexArea = new JTextArea();
	private MVCController controller; 
	
	
	public MVCView() {
		super();
		this.controller = new MVCController(this);
		panel();
	}
		
	public void panel() {
		showButton.addActionListener(this);
		JPanel centerPanel = new JPanel();
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout());
		this.setLayout(new BorderLayout());
		infoFlexArea.setSize(100, 100);
		infoFlexArea.setEditable(false);
		infoFlexArea.setBackground(centerPanel.getBackground());
		northPanel.add(infoFlexArea);
		centerPanel.setLayout(new FlowLayout());
		centerPanel.add(showButton);
		this.add((northPanel), BorderLayout.NORTH);
		this.add((centerPanel), BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	public void appendFlexareaText(String s) {
		final String ss = s;
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	infoFlexArea.append(ss);
		    }
		});
	}
	
		
	public void clearFlexArea() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	infoFlexArea.setText("");
		    }
		});
		
	}
	
	public void vaporizeButton() {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	showButton.setVisible(false);
		    }
		});
	}
	
	public void addImageToPanel() {
		final JPanel southPanel = new JPanel();
		
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	try {
					JLabel mcv1Label = new JLabel(new ImageIcon(ImageIO.read(this.getClass().getResource("mcv1.jpg"))));
					JLabel mcv2Label = new JLabel(new ImageIcon(ImageIO.read(this.getClass().getResource("mvc2.jpg"))));
					mcv1Label.setSize(50,50);
					southPanel.add(mcv1Label);
					southPanel.add(new JLabel("               "));
					southPanel.add(mcv2Label);
				} catch (IOException e) {
					e.printStackTrace();
				}
		    }
		});
		
		southPanel.removeAll();
		southPanel.revalidate();
		southPanel.repaint();
		this.add((southPanel), BorderLayout.SOUTH);
	}
	
	 
	
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == showButton) { //use new thread to call other class'. to avoid locking up swing
			Thread actionThread = new Thread() {
				public void run() {
					controller.startShow();
				}
			};
			actionThread.start();	
		}
	}
}