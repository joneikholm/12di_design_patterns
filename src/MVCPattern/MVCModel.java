package MVCPattern;

public class MVCModel {
	private String[] flexText = {"The MVC-pattern deals with setting up a GUI-Logic layer relationship.", "",
			"VIEW is the user interface. It should contain UI related functionality only. It knows about the Controller.",
			"CONTROLLER holds the functionality of the user interface, like what to do when a listener is triggered.", 
			"Controller is tied to the user interface as it holds its actions. Controller knows both Viev and Model.", 
			"MODEL is responsible of providing data. Model should not know neither Controller nor View, which allows", 
			"us to replace interfaces without possibly damaging our Model -Model holds hard to replace data."};
	private String[] endText = {"To the left, the standardized MVC pattern", 
			"To the right is a sketch of of an MVC implementation in some company system"};
	
	
	
	public String[] getFlexText() {
		return flexText;
	}
	
	public String[] getEndText() {
		return endText;
	}
	
	public MVCModel() {
	}
}	


