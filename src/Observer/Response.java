package Observer;

import java.util.*;

public class Response implements Observer
{
	public void update(Observable obj, Object arg)
	{
		if(!arg.equals(""))
			ObserverPanel.label.setText("Observer: " + arg);
		else
			ObserverPanel.label.setText("This label will observe your typing and change accordingly.");
	}
}
