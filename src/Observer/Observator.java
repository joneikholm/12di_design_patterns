package Observer;

import java.util.*;

public class Observator extends Observable implements Runnable
{
	public void run()
	{
		try
		{
			String oldText = new String();
			
			while(true)
			{
				if(!oldText.equals(ObserverPanel.text.getText()))
				{
					setChanged();
					oldText = ObserverPanel.text.getText();
					notifyObservers(oldText);
				}
			}
		}
		catch(Exception e) { }
	}
}