package Observer;

import javax.swing.*;

public class ObserverPanel extends JPanel
{
	public static JLabel label = new JLabel("This label will observe your typing and change accordingly.");
	public static JTextField text = new JTextField(10);
	private static final long serialVersionUID = 1L;

	public ObserverPanel()
	{
		super();
		
		String info = "<html>The observer pattern is a software design pattern in which an object, called the subject,<br>" +
				"maintains a list of its dependents, called observers, and notifies them automatically of<br>" +
				"any state changes, usually by calling one of their methods. It is mainly used to<br>" +
				"implement distributed event handling systems.</html>";
		JPanel bigPanel = new JPanel(), panel1 = new JPanel(), panel2 = new JPanel(), panel3 = new JPanel();
		bigPanel.setLayout(new BoxLayout(bigPanel, BoxLayout.PAGE_AXIS));
		panel1.add(new JLabel("Type: "));
		panel1.add(text);
		panel2.add(label);
		panel3.add(new JLabel(info));
		this.add(bigPanel);
		bigPanel.add(Box.createVerticalStrut(50));
		bigPanel.add(panel1);
		bigPanel.add(panel2);
		bigPanel.add(Box.createVerticalStrut(50));
		bigPanel.add(panel3);

		Observator observer = new Observator();
		observer.addObserver(new Response());
		new Thread(observer).start();
	}
}
