package Singleton;
  public class Singleton{

     private static Singleton instance=null;
     private static boolean ok=false;
     private Singleton(){
    	 ok=true;
     }

     public static Singleton getAnInstance(){
          if(instance==null)
              instance=new Singleton();

          return instance;
      }
     public static boolean checked(){
    	 return ok;
     }
     public static void setUnchecket(){
    	 ok=false;
     }
   }