package Singleton;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SingletonPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Singleton singleton;
	private static JLabel wIcon;
	private static JLabel wIconL;
	private static JLabel wIconR;
	private static JButton Client1;
	private static JButton Client2;
	private static JButton reset;
	private static JButton description;
	public SingletonPanel() {
		super();
		//  (nu inca)

		setLayout(null);
		setPreferredSize(new Dimension(600, 400));
		
		try {
			BufferedImage wPic = ImageIO.read(this.getClass().getResource(
					"db.jpg"));
			wIcon = new JLabel(new ImageIcon(wPic));
			wIcon.setBounds(120, 10, 90, 85);
			add(wIcon);
			wIcon.setVisible(false);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			BufferedImage wPic = ImageIO.read(this.getClass().getResource(
					"line.jpg"));
			wIconL = new JLabel(new ImageIcon(wPic));
			wIconL.setBounds(113, 77, 20, 50);
			add(wIconL);
			wIconL.setVisible(false);
			
			wIconR = new JLabel(new ImageIcon(wPic));
			wIconR.setBounds(197, 77, 20, 50);
			add(wIconR);
			wIconR.setVisible(false);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Client1 = new JButton("Client1");
		Client1.setBounds(25, 125, 100, 20);
		add(Client1);

		Client2 = new JButton("Client2");
		Client2.setBounds(200, 125, 100, 20);
		add(Client2);

		Client1.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				if (singleton.checked()==false) {
					singleton=singleton.getAnInstance();
					Client1.setEnabled(false);
					wIcon.setVisible(true);
					wIconL.setVisible(true);
				}else if(singleton.checked()!=false)
					wIconL.setVisible(true);
				Client1.setEnabled(false);
			}
		});
		Client2.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				if (singleton.checked()==false) {
					singleton=singleton.getAnInstance();
					Client2.setEnabled(false);
					wIcon.setVisible(true);
					wIconR.setVisible(true);
				} 
				else if(singleton.checked()!=false)
					{wIconR.setVisible(true);
					Client2.setEnabled(false);
					}
			}
		});
		description = new JButton("pattern description");
		description.setBounds(410, 290, 100, 20);
		add(description);
		description.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(
								null,
								" the singleton pattern is a design pattern that restricts the Instantiation of a\n" +
								" class to one object. This is useful when exactly one object is needed to coordinate\n" +
								" actions across the system. The concept is sometimes generalized to systems that operate\n" +
								" more efficiently when only one object exists, or that restrict the instantiation to a \n" +
								"certain number of objects. The term comes from the mathematical concept of a singleton.\n");

			}
		});
		
	    
	    reset = new JButton("reset");
	    reset.setBounds(200, 150, 100, 20);
		add(reset); 
		reset.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
					Client2.setEnabled(true);
					Client1.setEnabled(true);
					wIcon.setVisible(false);
					wIconR.setVisible(false);
					wIconL.setVisible(false);
					singleton.setUnchecket();
					
					}
			}
		);

		setVisible(true);
	}
	
}
