import java.awt.*;
import javax.swing.*;
//Patterns
import mediator.*;
import Observer.*;
import Iterator.*;
import Strategy.*;
import decorator.*;
import Singleton.*;
import Flyweight.*;
import MVCPattern.*;
import factoryPattern.*;

public class Panel
{
	public static void main(String[] args)
	{
		JTabbedPane tabbedPane = new JTabbedPane();
		JFrame frame = new JFrame("Patterns");
		frame.add(tabbedPane);
		
		// Add your JPanel to the 'tabbedPane' with addTab(String, JPanel);
		tabbedPane.addTab("Mediator", new MediatorPanel());
		tabbedPane.addTab("Observer", new ObserverPanel());
		tabbedPane.addTab("Iterator", new IteratorPanel());
		tabbedPane.addTab("Factory", new hitesh_Factory_Pattern());
		tabbedPane.addTab("Strategy", new StrategyPanel());
		tabbedPane.addTab("Decorator", new DecoratorPanel());
		tabbedPane.addTab("MVC", new MVCView());
		tabbedPane.addTab("Singleton", new SingletonPanel());
		tabbedPane.addTab("Flyweight", new FlyweightPanel());
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(650, 400));
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
